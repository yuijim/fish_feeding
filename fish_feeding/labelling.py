import cv2
import numpy
import os
import csv

try:
    from skvideo.io import VideoCapture
except ImportError:
    from cv2 import VideoCapture

datafiles = [
    '../data/20150208_11-02-08.mpg',
    '../data/20150302_10-40-14.mpg',
    '../data/20150319_14-35-13.mpg',
    '../data/20150319_14-40-38.mpg',
    '../data/20150705_13-18-17.mpg'
]

MAX_BREAK_ALLOWED = 10


def find_indexes(channel):
    indexes = numpy.nonzero(numpy.diff(numpy.concatenate(([0], channel))))[0]

    if indexes.shape[0] % 2 == 1:
        indexes = numpy.append(indexes, [channel.shape[0]])

    indexes = list(indexes.reshape((-1, 2)))

    filtered = [[indexes[0][0], indexes[0][1], channel[indexes[0][0]]]]

    for i in range(len(indexes) - 1):
        if indexes[i + 1][0] - indexes[i][1] <= MAX_BREAK_ALLOWED:
            filtered[-1][1] = indexes[i + 1][1]
        else:
            filtered.append([indexes[i + 1][0], indexes[i + 1][1], channel[indexes[i + 1][0]]])

    with_zeros = []
    if filtered[0][0]:  # video starts with eating
        with_zeros.append([0, filtered[0][0], 0])
    for i in range(len(filtered) - 1):
        with_zeros.append(filtered[i])
        with_zeros.append([filtered[i][1], filtered[i+1][0], 0])
    with_zeros.append(filtered[-1])
    if filtered[-1][1] != channel.shape[0]:  # video ends with eating
        with_zeros.append([filtered[-1][1], channel.shape[0], 0])

    return with_zeros


def run(path):
    cap = VideoCapture(path)
    labels = []
    color = None
    alpha = 0.3

    if cap.isOpened():
        run = True
        first = True
        while run:
            ret, frame = cap.read()

            overlay = frame.copy()
            output = frame.copy()

            if color is not None:
                cv2.rectangle(overlay,
                              (frame.shape[1] - 70, frame.shape[0] - 70),
                              (frame.shape[1] - 20, frame.shape[0] - 20),
                              color, -1)
                cv2.addWeighted(overlay, alpha, output, 1 - alpha, 0, output)

            cv2.imshow('fish_feeding', output)

            if ret:
                if first:
                    key = cv2.waitKey(0) & 0xFF
                    first = False
                else:
                    key = cv2.waitKey(1) & 0xFF

                if key == ord('q'):  # quit video
                    break
                elif key == ord(' '):  # not eating
                    labels.append(1)
                    color = (0, 0, 255)
                elif key == ord('i'):  # ignore
                    labels.append(-1)
                    color = (0, 0, 0)
                elif key == ord('p'):
                    first = True
                    labels.append(labels[-1])  # repeat last
                else:
                    labels.append(0)  # eating
                    color = None
            else:
                # ignore those frames
                labels.append(-1)
                color = (0, 0, 0)

        cap.release()

        if len(labels):
            indexes = find_indexes(numpy.array(labels))

            labels_file = open(os.path.splitext(path)[0] + '.csv', 'w')
            writer = csv.writer(labels_file)
            writer.writerows(indexes)
            labels_file.close()

    cv2.destroyAllWindows()


if __name__ == "__main__":
    for video in datafiles:
        run(video)
